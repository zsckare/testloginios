//
//  ViewController.swift
//  Login2
//
//  Created by k@tty on 22/06/16.
//  Copyright © 2016 k@tty. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var userEmailLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func btnLogOut(sender: UIButton) {
        //Almacenar Datos
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUserLoggedIn");
        NSUserDefaults.standardUserDefaults().synchronize();
        self.performSegueWithIdentifier("loginView", sender: self)
    }
    override func viewDidAppear(animated: Bool) {
        
        let userLoggedIn = NSUserDefaults.standardUserDefaults().boolForKey("isUserLoggedIn");
        if(!userLoggedIn){
             self.performSegueWithIdentifier("loginView", sender: self)
            setWelcomeMessage();
        }
       
    }
    
    func setWelcomeMessage(){
        let userEmail = NSUserDefaults.standardUserDefaults().stringForKey("userName")
        let userID = NSUserDefaults.standardUserDefaults().integerForKey("userId")
        let usrID = "userID \(userID)"
        print(usrID)
        userEmailLabel.text=userEmail;
    }
}

