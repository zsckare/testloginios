//
//  DriversViewController.swift
//  Login2
//
//  Created by k@tty on 22/06/16.
//  Copyright © 2016 k@tty. All rights reserved.
//

import UIKit
import GoogleMaps
import Foundation
import Alamofire

class DriversViewController: UIViewController,CLLocationManagerDelegate {

    var manager:CLLocationManager!
    var mainLatitude: Double = 0.0
    var mainLongitude: Double = 0.0
    var bandera: Int = 0
    let userID = NSUserDefaults.standardUserDefaults().integerForKey("userId")
    
    //var alert: UIAlertController!
     var actionString: String?
    @IBOutlet weak var mapViewMain: GMSMapView!
    
    @IBOutlet weak var referenciasTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        
        
        let alertViewController = UIAlertController(title: "Test Title", message: "userID \(userID)", preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: "OK", style: .Default) { (action) -> Void in
            self.actionString = "OK"
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) -> Void in
            self.actionString = "Cancel"
        }
        
        alertViewController.addAction(okAction)
        
        dispatch_async(dispatch_get_main_queue(), {
            self.presentViewController(alertViewController, animated: true, completion: nil)
        })
       
        getDrivers()
        
       // alert = UIAlertController(title: "TEST", message: "HI", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func centerMap(){
       
        //print("\(mainLatitude),\(mainLongitude)")
        let camera = GMSCameraPosition.cameraWithLatitude(mainLatitude, longitude: mainLongitude, zoom: 13)
        mapViewMain.camera = camera
        
        bandera=1
        
    }
    
    func getDrivers(){
        let uri = "http://localhost:3000/api/v1/drivers"
        let urlRequest = NSURL(string: uri)
        
        NSURLSession.sharedSession().dataTaskWithURL(urlRequest!){ (data,response,error) in
            
            if( error != nil) {
                print(error)
                return
            }
            
            do{
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers)
                for dictionary in json as! [[String: AnyObject]]{
                    
                    
                    var latlng = dictionary["latlon"]
                    var numeroEconomico = dictionary["economico"]
                    var latlngArray = latlng!.componentsSeparatedByString(",")
                    
                    let latitude = (latlngArray[0] as NSString).doubleValue
                    let longitude = (latlngArray[1] as NSString).doubleValue
                    
                    let currentLocation = CLLocationCoordinate2DMake(latitude,longitude)
                   
                    
                    dispatch_async(dispatch_get_main_queue(),{
                        let marker = GMSMarker(position: currentLocation)
                        marker.title = numeroEconomico! as! String
                        marker.map = self.mapViewMain
                    })
                }
                
                print(json)
                
            }catch let jsonError{
                print(jsonError)
            }
            
            }.resume()
    }
    
    func locationManager(manager:CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        print(" -----> \(location!.coordinate.latitude) , \(location!.coordinate.longitude)")
        
        mainLongitude = location!.coordinate.longitude
        mainLatitude = location!.coordinate.latitude
        if (bandera==0) {
            centerMap()
        }
        
        
    }
    
    @IBAction func btnPedir(sender: UIButton) {
        pedirTaxi()
    }
   
    
    func pedirTaxi(){
        let uri = "http://localhost:3000/api/v1/services"
        let referencias = referenciasTextField.text!
        let latlng = "\(mainLatitude),\(mainLongitude)"
        let parameter = [
            "service": [
                "client_id": userID,
                "latlon": latlng,
                "referencia": referencias
            ]
        ]
        
        let parameters = "service[client_id]=\(userID)&service[latlon]=\(latlng)&service[referencia]=\(referencias)"
        
        let url:NSURL = NSURL(string: uri)!
        let session = NSURLSession.sharedSession()
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        
        
        let data = parameters.dataUsingEncoding(NSUTF8StringEncoding)
        
        
        let task = session.uploadTaskWithRequest(request, fromData: data, completionHandler:
            {(data,response,error) in
                
                guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                    print("error")
                    return
                }
                
                let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                
                print(dataString)
                
            }
        );
        
        task.resume()
    }
    


}
